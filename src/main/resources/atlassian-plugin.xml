<!--
      Copyright © 2012 - 2013 Atlassian Corporation Pty Ltd.

      Licensed under the Apache License, Version 2.0 (the "License");
      you may not use this file except in compliance with the License.
      You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

      Unless required by applicable law or agreed to in writing, software
      distributed under the License is distributed on an "AS IS" BASIS,
      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
      See the License for the specific language governing permissions and
      limitations under the License.
 -->
<atlassian-plugin key="${project.groupId}.${project.artifactId}" name="${project.name}" plugins-version="2">
    <plugin-info>
        <description>${project.description}</description>
        <version>${project.version}</version>
        <vendor name="${project.organization.name}" url="${project.organization.url}" />
    </plugin-info>

    <component-import key="userManager" interface="com.atlassian.sal.api.user.UserManager" />

    <component name="Source Editor Configuration Manager"
               key="sourceEditorConfigurationManager"
               class="com.atlassian.confluence.plugins.editor.source.settings.DefaultSourceEditorConfigurationManager"/>

    <web-resource key="atlassian-source-editor-javascript" name="Atlassian Source Editor Javascript Resources">
        <transformation extension="soy">
            <transformer key="soyTransformer"/>
        </transformation>

        <resource name="codemirror.js" type="download" location="jscripts/CodeMirror-2.21/lib/codemirror.js"/>
        <resource name="codemirror-mode-xml.js" type="download" location="jscripts/CodeMirror-2.21/mode/xml/xml.js"/>
        <resource name="codemirror-search-cursor.js" type="download" location="jscripts/CodeMirror-2.21/lib/util/searchcursor.js"/>

        <resource name="source-editor-templates.js" type="download" location="templates/source-editor-templates.soy"/>
        <resource name="source-editor.js" type="download" location="jscripts/source-editor/source-editor.js"/>
        <resource name="source-editor-cm-search-adapter.js" type="download" location="jscripts/source-editor/source-editor-cm-search-adapter.js"/>
        <resource name="source-editor-search.js" type="download" location="jscripts/source-editor/source-editor-search.js"/>
    </web-resource>

    <web-resource key="atlassian-source-editor-css" name="Atlassian Source Editor CSS Resources">
        <resource name="codemirror.css"                          type="download" location="jscripts/CodeMirror-2.21/lib/codemirror.css"/>
        <resource name="source-editor.css"                       type="download" location="css/source-editor.css"/>
        <context>editor</context>
    </web-resource>

    <web-resource key="atlassian-source-editor-resources" name="Atlassian Source Editor Resources">
        <dependency>${project.groupId}.${project.artifactId}:atlassian-source-editor-javascript</dependency>
        <dependency>com.atlassian.auiplugin:ajs</dependency>
        <context>editor</context>
        <condition class="com.atlassian.confluence.plugins.editor.source.condition.SourceEditorCondition"/>
    </web-resource>

    <rest key="restEndPoint" path="/sourceeditor" name="Atlassian Source Editor Rest Endpoint" version="1.0"
            description="Provides REST resources"/>

    <!-- Admin -->

    <web-resource key="atlassian-source-editor-admin" name="Atlassian Source Editor Javascript Resources">
        <resource name="source-editor-admin.js"  type="download" location="jscripts/source-editor/source-editor-admin.js"/>
        <resource name="source-editor-admin.css" type="download" location="css/source-editor-admin.css"/>
    </web-resource>

    <web-item key="atlassian-source-editor-admin-menu" name="Source Editor" section="system.admin/configuration"  weight="40">
        <label>Source Editor</label>
        <link>/admin/sourceeditor/viewconfiguration.action</link>
        <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.ConfluenceAdministratorCondition"/>
    </web-item>

    <xwork name="atlassian-source-editor-admin-xwork" key="atlassian-source-editor-admin-xwork">
        <description>Provides administration support for the Atlassian Source Editor.</description>

        <package name="atlassian-source-editor-admin" extends="default" namespace="/admin/sourceeditor">
            <default-interceptor-ref name="validatingStack" />
            <action name="viewconfiguration" class="com.atlassian.confluence.plugins.editor.source.action.ViewConfigurationAction">
                <result name="success" type="velocity">/templates/source-editor-configuration.vm</result>
            </action>
            <action name="updateconfiguration" class="com.atlassian.confluence.plugins.editor.source.action.UpdateConfigurationAction">
                <result name="success" type="redirect">viewconfiguration.action</result>
            </action>
        </package>
    </xwork>

    <!-- View storage format -->

    <web-resource key="atlassian-source-editor-view-storage-javascript" name="Atlassian Source Editor View Storage Javascript Resources">
        <resource name="source-editor-view-source.js" type="download" location="jscripts/source-editor/view-source.js"/>
        <context>page</context>
        <context>blogpost</context>
        <dependency>confluence.web.resources:ajs</dependency>
    </web-resource>

    <web-item key="source-editor-view-storage" name="View Storage Format Link" section="system.content.action/secondary" weight="20">
        <label>View Storage Format</label>
        <link linkId="action-source-editor-view-storage-link">/plugins/viewstorage/viewpagestorage.action?pageId=$helper.page.id</link>
        <conditions type="AND">
            <condition class="com.atlassian.confluence.plugin.descriptor.web.conditions.PagePermissionCondition">
                <param name="permission">view</param>
            </condition>
            <condition class="com.atlassian.confluence.plugins.editor.source.condition.SourceEditorCondition"/>
        </conditions>
        <styleClass>action-view-storage view-storage-link popup-link</styleClass>
    </web-item>

</atlassian-plugin>
